#
# Test the curves.
# - creates a graph using matplotlib

from __future__ import division
from __future__ import print_function

from numeric_interpolation import *

print("Numeric interp")
# For a duration 10 you will get the relevant output from start to end
a = Quad_ease_in_out([0,1], [0,3])
#result is the returned value from start to end (0 to 3)
print(a, a.interpolate(0.5))

# slerp check
print("\nRotation (Slerp) interp:")
b = Quad_ease_in_out([0,1], [[0,0,0], [-88,0,88]], mode='rotation')
print(b)
for i in [0,0.05,1]:
	print("at %2.2f: %s" %(i, b.interpolate(i)))
#
# color check
print("\nColor interp:")
c = Quad_ease_in_out([0,1], [[45,30,45], [255,128,0]], mode='color')
print(c)
for i in [0,0.05,0.5,1]:
	print("at %2.2f: %s" %(i, c.interpolate(i)))
#
# numeric check
print("\nNumeric interp:")
c = Quad_ease_in_out([0,1], [[45,30,45], [255,128,0]])
print(c)
for i in [0,0.05,0.5,1]:
	print("at %2.2f: %s" %(i, c.interpolate(i)))


# example plots:

import numpy as np
import matplotlib.pyplot as plt

methods = [ Numeric_interpolator, 
			Quad_ease_in_out, Quad_ease_in, Quad_ease_out,
	Cubic_ease_in, Cubic_ease_in, Cubic_ease_out,
	Quartic_ease_in,Quartic_ease_out,Quartic_ease_in_out,
	Quintic_ease_in,Quintic_ease_out,Quintic_ease_in_out,
	Sine_ease_in,Sine_ease_out,Sine_ease_in_out,
	Circular_ease_in,Circular_ease_out,Circular_ease_in_out,
	Exponential_ease_in,Exponential_ease_out,Exponential_ease_in_out,
	Elastic_ease_in,Elastic_ease_out,Elastic_ease_in_out,
	Back_ease_in, Back_ease_out,Back_ease_in_out,
	Bounce_ease_in_out, Bounce_ease_in, Bounce_ease_out]

# Set the in and out ranges to test the eases
inrange = [-10,10]
outrange = [2,-5]

### Test
results = []
graphs = []
for method in methods:
	results.append(method(inrange, outrange))
	
### Graph against the X as inrange
x = np.arange(inrange[0], inrange[1], 1)#0.0199)
for res in results:
	graphs.append(list(map(res.interpolate, x)))
for g in graphs:
	plt.plot(x, g)
plt.show()
print(len(x),x)
print(len(graphs[0]),graphs)