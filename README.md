# Interpolations

A set of eases or interpolations to be used standalone or in the animation system

A general Numeric Interpolator is the principle. It takes:
- a tuple of inrange - two numeric values defining the input range to the Interpolator.
  - Generally this is implemented as time. As such the second value must be higher than the first.
- a tuple of outrange - defining the range of values mapped to the input range.
  - this can be a single value or a list of values. If lists then they must be the same length.
  - Generally this is a single value, or a pair for 2D operations, or a triplet for 3D operations and colours.
- a mode - this defines special cases (hints) for how to interpolate the values.
  - numeric  (default) - simple numeric interpolation.
  - color - expecting RGB triplets in form of bytes (0-255) or floats (0.0-1.0).
     - this performs conversion from RGB to LHC space, does the interpolation there, and returns an RGB.
  - rotation - expecting a triplet for 3D rotation.
     - this converts the euler angle representations to Quaternions, interpolates, then returns a Euler angle triplet.

This general linear interpolation method is subclassed for each of the standard curves used to ease values. A general Interpolator class is also generated that takes a curve as an argument for ease of use in some situations.

Classes are:
- Numeric_interpolator (default linear interpolation),
- Quad_ease_in_out, Quad_ease_in, Quad_ease_out,
- Cubic_ease_in, Cubic_ease_out, Cubic_ease_in_out,
- Quartic_ease_in, Quartic_ease_out, Quartic_ease_in_out,
- Quintic_ease_in, Quintic_ease_out, Quintic_ease_in_out,
- Sine_ease_in, Sine_ease_out, Sine_ease_in_out,
- Circular_ease_in, Circular_ease_out, Circular_ease_in_out,
- Exponential_ease_in, Exponential_ease_out, Exponential_ease_in_out,
- Elastic_ease_in, Elastic_ease_out, Elastic_ease_in_out,
- Back_ease_in, Back_ease_out, Back_ease_in_out,
- Bounce_ease_in, Bounce_ease_out, Bounce_ease_in_out

and the general purpose:
- Interpolator
	
E.g.
```python
# For inrange 0 to 9 (e.g. duration 10) results in the interpolated output from outrange.
a = Quad_ease_in_out([0,9], [0,3])
# Result is the returned value from outrange (0 to 3)
print a, a.interpolate(4.5)

<Quad_ease_in_out: in=[0,9] out=[0,3]>, 1.5
```
![graph](resources/Interpolation_Curves-01.png)
# Demos
- demo_interp_usage.py shows code usage and builds the graph image.
- demo_opengl_interp.py shows the Interpolator in use in an opengl torus program with mouse and keyboard input.
    - Use the Left and Right arrow keys to select an interpolation curve. It will cycle through the curves.
	- Use Mouse-L to select a place on screen for the torus to translate to. It will use the selected interpolation curve.
- demo_2Danimation.py compares the curves used in 2D screen space
    - Use the Left and Right arrow keys to select a set of curves.
	- Use Mouse-L to select a place on screen for the dinosaur to move to.
