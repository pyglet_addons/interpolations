### Interpolation functions

### Usage:
### To animate a value, create a Numeric_interpolator populating it with the initial in and out ranges.
### E.g. For an inrange 0 to 10 (e.g. duration 10) results in the interpolated output from outrange.
###      a = Quad_ease_in_out([0,10], [0,3])
###      Result is the returned value from outrange (0 to 3)
###      print(a, a.interpolate(5)) = 1.5

### Get the interpolated result by calling interpolate(value_inside_inrange)
### Note that inrange must be a pair in increasing order (usually time)
###  but outrange can be any two numbers (or lists of numbers).
### If either range changes, override with set_in or set_out.
### Mode defines the type of interpolation:
###  - numeric - (default)straight numeric interp on single number or list of numbers.
###  - color - assumes color triplet as byte or float. 
###          - does lhc color interp for best result.
###  - rotation - assumes outrange is a triplet.
###             - does spherical interpolation in quaternion space (slerp).

### A general pupose class is also available where the interp curve is an argument
### E.g. Interpolator(Quad_ease_in_out, [30,150], [-2,5])
###      is equivalent to:  Quad_ease_in_out([30,150], [-2,5])

### E.g. quadratic interp of animation where time is between 30-150 and the result lies between -2 and 5.
###    interp = Quad_ease_in_out([30,150], [-2,5])
###   at time = 66
###    newvval = interp.interpolate(66)

### E.g. interpolate three numbers at the same time (E.g. xyz pos) over the same time interval
###    interp = Quad_ease_in_out([0,1], [[-50,50,0], [-1.0, 2.3, 4.8]])
###   so interp.interpolate(0.5)
###    = [-44.077465277777776, 44.23459375, 0.5801666666666666]
### If the animatable property needs to change, and is now using an outrange of 10 to 20 then:
###    interp.set_out(10,20)
###   so interp.interpolate(0.5)
###    = 11.2086805556

### Available mnethods are:
"""	Numeric_interpolator, 
	Quad_ease_in_out, Quad_ease_in, Quad_ease_out,
	Cubic_ease_in, Cubic_ease_in, Cubic_ease_out,
	Quartic_ease_in,Quartic_ease_out,Quartic_ease_in_out,
	Quintic_ease_in,Quintic_ease_out,Quintic_ease_in_out,
	Sine_ease_in,Sine_ease_out,Sine_ease_in_out,
	Circular_ease_in,Circular_ease_out,Circular_ease_in_out,
	Exponential_ease_in,Exponential_ease_out,Exponential_ease_in_out,
	Elastic_ease_in,Elastic_ease_out,Elastic_ease_in_out,
	Back_ease_in, Back_ease_out,Back_ease_in_out,
	Bounce_ease_in_out, Bounce_ease_in, Bounce_ease_out
"""

from __future__ import division
from __future__ import print_function

from math import pi, sin, cos, sqrt, pow, atan2
from quaternions import *

# for color conversion
_oneThird = 1.0 / 3
_srgbGammaCorrInv = 0.03928 / 12.92
_sixteenHundredsixteenth = 16.0 / 116
_24th = 1/2.4
# _Xn = 0.950470
# _Yn = 1
# _Zn = 1.088830

class Numeric_interpolator:
	""" Actually Linear interpolation over the range defined by inrange,
		Giving results in the range defined by outrange.
		- outrange may be a list of tuples or lists.
		Inrange is generally time and outrange the value or values to result in
	"""
	def __init__(self, inrange=[0,1], outrange=[0,1], mode='numeric'):
		""" mode can be:
			- numeric - single or list of numbers
			- rotation - triplet of degrees
			- color_byte, color_float. 0..255 or 0..1 triplet
		"""
		self.out_start,self.out_end = outrange
		self.in_start,self.in_end = inrange  # time
		self.duration = self.in_end - self.in_start
		self.mode = mode
		# specials for the special modes (to save computation time)
		self.quaternion_1 = None
		self.quaternion_2 = None
		self.lch_color_1 = None
		self.lch_color_2 = None

	def __repr__(self):
		return "<%s: in=[%s,%s] out=[%s,%s]>"%(self.__class__.__name__, self.in_start, self.in_end, self.out_start,self.out_end)
	
	def set_in(self, in_start, in_end):
		" to change the time range after creation "
		self.in_start = in_start
		self.in_end = in_end
		self.duration = self.in_end - self.in_start
		# clear special caches
		self.quaternion_1 = None
		self.lch_color_1 = None

	def set_out(self, out_start, out_end):
		" to change the output range after creation "
		self.out_start = out_start
		self.out_end = out_end
		# clear special caches
		self.quaternion_2 = None
		self.lch_color_2 = None
		
	def time_func(self, t):
		""" Linear by default
			Override and returning a modified verison of t 
			Expected to be in range 0..1
		"""
		return t

	def interpolate(self, alpha):
		" alpha must lie within inrange "
		# remap into range 0..1
		alpha = (alpha - self.in_start)/ float(self.duration)
		a = self.time_func(alpha) # normalised to 0..1
		# check for lists vs single values
		if not isinstance(self.out_end, list):
			# single value numeric
			return self.out_end * a + self.out_start * (1 - a)
		else: # list expected
			if self.mode == 'numeric':
				# regular numeric interp
				return [self.out_end[i]*a + self.out_start[i]*(1 - a) for i in range(len(self.out_end))]
			elif self.mode == 'rotation':
				# do spherical interpolate on a triplet
				if len(self.out_end) == 3:
					if self.quaternion_1 == None:
						self.quaternion_1 = Quat()
						x = self.out_start[0]
						y = self.out_start[1]
						z = self.out_start[2]
						self.quaternion_1.set_from_euler_angles(x,y,z)
					if self.quaternion_2 == None:
						self.quaternion_2 = Quat()
						x = self.out_end[0]
						y = self.out_end[1]
						z = self.out_end[2]
						self.quaternion_2.set_from_euler_angles(x,y,z)
					qnew = Quat()
					qnew.slerp(self.quaternion_1, self.quaternion_2, a)
					eulers = qnew.get_euler_angles(None)
					return eulers
			elif self.mode == 'color':
				if len(self.out_end) == 3:
					if self.lch_color_1 == None:
						self.lch_color_1 = rgb2lch(self.out_start)
					if self.lch_color_2 == None:
						self.lch_color_2 = rgb2lch(self.out_end)
					#
					diff = self.lch_color_2[2] - self.lch_color_1[2]
					if abs(diff) > 180:
						if diff > 0 :self.lch_color_1[2] += 360
						else: self.lch_color_2[2] += 360
					lch = [self.lch_color_2[i]*a + self.lch_color_1[i]*(1 - a) for i in range(len(self.lch_color_2))]
					# print "",diff, self.lch_color_1,self.lch_color_2, lch
					return lch2rgb(lch)

###
# could use the colorsys module - colorsys.rgb_to_hsv(r, g, b)
###
def rgb2lch(rgb_triplet):
	""" rgb to xyz, to lab, to lch
		_Xn etc are not really needed for this they just get to D65 LAB white balance
	"""
	color = rgb_triplet # assume float not byte color supplied
	if not isinstance(rgb_triplet[0], float):
		color = [v/255.0 for v in rgb_triplet]
	#print "RGB %s"%(color)
	r, g, bl = [((v <= 0.03928) and [v / 12.92] or [((v+0.055) / 1.055) **2.4])[0] for v in color]
	x = (r * 0.4124 + g * 0.3576 + bl * 0.1805)# / _Xn
	y = (r * 0.2126 + g * 0.7152 + bl * 0.0722)# / _Yn
	z = (r * 0.0193 + g * 0.1192 + bl * 0.9505)# / _Zn
	#print "XYZ # %2f %2f %2f"%(x*Xn,y*Yn,z*Zn)
	x,y,z = [((v > 0.008856) and [v**_oneThird] or [(7.787 * v) + _sixteenHundredsixteenth])[0] for v in (x, y, z)]
	#
	l = 116 * y - 16
	a = 500 * (x - y)
	b = 200 * (y - z)
	#
	c = sqrt(a * a + b * b)
	h = (degrees(atan2(b, a)) + 360) % 360
	#print "LAB %2f %2f %2f \nLCH # %2f %2f %2f"%(l,a,b,l,c,h)
	return [l,c,h]

def lch2rgb(triplet, return_byte=True):
	l,c,h = triplet
	h = radians(h)
	a = cos(h) * c
	b = sin(h) * c
	#print "LAB %2f %2f %2f"%(l,a,b)
	#
	y = (l + 16)/116.0
	x = y + a/500.0
	z = y - b/200.0
	#
	x,y,z = [((v > 0.206893) and [v**3] or [(v - _sixteenHundredsixteenth) / 7.787])[0] for v in [x, y, z]]
	# x *= _Xn
	# y *= _Yn
	# z *= _Zn
	#print "XYZ %2f %2f %2f"%(x,y,z)
	r  =  3.2404542 * x - 1.5371385 * y - 0.4985314 * z  # D65 -> sRGB
	g  = -0.9692660 * x + 1.8760108 * y + 0.0415560 * z
	bl =  0.0556434 * x - 0.2040259 * y + 1.0572252 * z
	col = [((v <= _srgbGammaCorrInv) and [v * 12.92] or [(1.055 * (v**_24th)) - 0.055])[0] for v in (r, g, bl)]
	if return_byte:
		r,g,bl = [int(round(255*a)) for a in col]
	else:
		r,g,bl = col
	#print "RGB # %2d %2d %2d"%(r,g,bl)
	return [r,g,bl]

	

### Quadratic easing functions
class Quad_ease_in_out(Numeric_interpolator):
	def time_func(self, t):
		if t < 0.5:
			return 2 * t * t
		return (-2 * t * t) + (4 * t) - 1

class Quad_ease_in(Numeric_interpolator):
	def time_func(self, t):
		return t * t

class Quad_ease_out(Numeric_interpolator):
	def time_func(self, t):
		return -(t * (t - 2))

### Cubic easing functions
class Cubic_ease_in(Numeric_interpolator):
	def time_func(self, t):
		return t * t * t

class Cubic_ease_out(Numeric_interpolator):
	def time_func(self, t):
		return (t - 1) * (t - 1) * (t - 1) + 1

class Cubic_ease_in_out(Numeric_interpolator):
	def time_func(self, t):
		if t < 0.5:
			return 4 * t * t * t
		p = 2 * t - 2
		return 0.5 * p * p * p + 1

### Quartic easing functions
class Quartic_ease_in(Numeric_interpolator):
	def time_func(self, t):
		return t * t * t * t

class Quartic_ease_out(Numeric_interpolator):
	def time_func(self, t):
		return (t - 1) * (t - 1) * (t - 1) * (1 - t) + 1

class Quartic_ease_in_out(Numeric_interpolator):
	def time_func(self, t):
		if t < 0.5:
			return 8 * t * t * t * t
		p = t - 1
		return -8 * p * p * p * p + 1

### Quintic easing functions
class Quintic_ease_in(Numeric_interpolator):
	def time_func(self, t):
		return t * t * t * t * t

class Quintic_ease_out(Numeric_interpolator):
	def time_func(self, t):
		return (t - 1) * (t - 1) * (t - 1) * (t - 1) * (t - 1) + 1

class Quintic_ease_in_out(Numeric_interpolator):
	def time_func(self, t):
		if t < 0.5:
			return 16 * t * t * t * t * t
		p = ((2 * t) - 2)
		return 0.5 * p * p * p * p * p + 1

### Sine easing functions
class Sine_ease_in(Numeric_interpolator):
	def time_func(self, t):
		return sin((t - 1) * pi / 2) + 1

class Sine_ease_out(Numeric_interpolator):
	def time_func(self, t):
		return sin(t * pi / 2)

class Sine_ease_in_out(Numeric_interpolator):
	def time_func(self, t):
		return 0.5 * (1 - cos(t * pi))

### Circular easing functions
class Circular_ease_in(Numeric_interpolator):
	def time_func(self, t):
		return 1 - sqrt(1 - (t * t))

class Circular_ease_out(Numeric_interpolator):
	def time_func(self, t):
		return sqrt((2 - t) * t)

class Circular_ease_in_out(Numeric_interpolator):
	def time_func(self, t):
		if t < 0.5:
			return 0.5 * (1 - sqrt(1 - 4 * (t * t)))
		return 0.5 * (sqrt(-((2 * t) - 3) * ((2 * t) - 1)) + 1)

### Exponential easing functions
class Exponential_ease_in(Numeric_interpolator):
	def time_func(self, t):
		if t == 0:
			return 0
		return pow(2, 10 * (t - 1))

class Exponential_ease_out(Numeric_interpolator):
	def time_func(self, t):
		if t == 1:
			return 1
		return 1 - pow(2, -10 * t)

class Exponential_ease_in_out(Numeric_interpolator):
	def time_func(self, t):
		if t == 0 or t == 1:
			return t
		if t < 0.5:
			return 0.5 * pow(2, (20 * t) - 10)
		return -0.5 * pow(2, (-20 * t) + 10) + 1

### Elastic Easing Functions
class Elastic_ease_in(Numeric_interpolator):
	def time_func(self, t):
		return sin(13 * pi / 2 * t) * pow(2, 10 * (t - 1))

class Elastic_ease_out(Numeric_interpolator):
	def time_func(self, t):
		return sin(-13 * pi / 2 * (t + 1)) * pow(2, -10 * t) + 1

class Elastic_ease_in_out(Numeric_interpolator):
	def time_func(self, t):
		if t < 0.5:
			return 0.5 * sin(13 * pi / 2 * (2 * t)) * pow(2, 10 * ((2 * t) - 1))
		return 0.5 * (sin(-13 * pi / 2 * ((2 * t - 1) + 1)) * pow(2, -10 * (2 * t - 1)) + 2)

### Back Easing Functions
class Back_ease_in(Numeric_interpolator):
	def time_func(self, t):
		return t * t * t - t * sin(t * pi)

class Back_ease_out(Numeric_interpolator):
	def time_func(self, t):
		p = 1 - t
		return 1 - (p * p * p - p * sin(p * pi))

class Back_ease_in_out(Numeric_interpolator):
	def time_func(self, t):
		if t < 0.5:
			p = 2 * t
			return 0.5 * (p * p * p - p * sin(p * pi))
		p = (1 - (2 * t - 1))
		return 0.5 * (1 - (p * p * p - p * sin(p * pi))) + 0.5

### Bounce Easing Functions
class Bounce_ease_in(Numeric_interpolator):
	def time_func(self, t):
		return 1 - Bounce_ease_out().time_func(1 - t)

class Bounce_ease_out(Numeric_interpolator):
	def time_func(self, t):
		if t < 4/11.0:
			return 121 * t * t / 16.0
		elif t < 8/11.0:
			return (363 / 40.0 * t * t) - (99 / 10.0 * t) + 17 / 5.0
		elif t < 9/10.0:
			return (4356 / 361.0 * t * t) - (35442 / 1805.0 * t) + 16061 / 1805.0
		return (54 / 5.0 * t * t) - (513 / 25.0 * t) + 268 / 25.0
# class Bounce_ease_out(Numeric_interpolator):
	# def time_func(self, t):
		# if t < 0.36:
			# return 121 * t*t / 16.0
		# elif t < 0.72:
			# return (9.075 * t*t) - (10 * t) + 3.4
		# elif t < 0.9:
			# return (11.789 * t*t) - (19.635 * t) + 8.89
		# return (10.8 * t*t) - (20.52 * t) + 10.72
	
class Bounce_ease_in_out(Numeric_interpolator):
	def time_func(self, t):
		if t < 0.5:
			return 0.5 * Bounce_ease_in().time_func(t * 2)
		return 0.5 * Bounce_ease_out().time_func(t * 2 - 1) + 0.5

		
### class for an alternate dispatch mechanism
### where the interpolation function is an argument
class Interpolator(object):
			
	def __init__(self, method, inrange=[0,1], outrange=[0,1], mode='numeric'):
		self.interpolator = method(inrange, outrange, mode)
	
	def __repr__(self):
		return "%s"%(self.interpolator)
		
	def interpolate(self, alpha):
		return self.interpolator.interpolate(alpha)
	
	def set_in(self, in_start, in_end):
		self.interpolator.set_in(in_start, in_end)

	def set_out(self, out_start, out_end):
		self.interpolator.set_out(out_start, out_end)
	
		
### Testing
if __name__ == "__main__":
	import random
	size = 10
	count = 0
	print("Checking Color conversion:")
	for i in range(size):
		color = [random.randint(0,255) for i in range(3)]
		lch = rgb2lch(color)
		rgb = lch2rgb(lch)
		if color != rgb:
			count += 1
			print(color, lch, rgb)
	print(" %d random colors checked rgb->lch->rgb.\n - %d Failed" %(size,count))
	#
	print("Checking float colors:")
	color = [random.random() for i in range(3)]
	lch = rgb2lch(color)
	rgb = lch2rgb(lch, False)
	print("Initial color: %s\n       to lch: %s\n    to color: %s" %(color, lch, rgb))
	#
	# slerp check
	print("\nRotation (Slerp) interp:")
	b = Quad_ease_in_out([0,1], [[0,0,0], [-88,0,88]], mode='rotation')
	print(b)
	for i in [0,0.05,0.5,1]:
		print("at %2.2f: %s" %(i, b.interpolate(i)))
	#
	# color check
	print("\nColor interp:")
	c = Quad_ease_in_out([0,1], [[45,30,45], [255,128,0]], mode='color')
	print(c)
	for i in [0,0.05,0.5,1]:
		print("at %2.2f: %s" %(i, c.interpolate(i)))
	#
	# numeric check
	print("\nNumeric interp (compare to color):")
	c = Quad_ease_in_out([0,1], [[45,30,45], [255,128,0]])
	print(c)
	for i in [0,0.05,0.5,1]:
		print("at %2.2f: %s" %(i, c.interpolate(i)))
	#
	# numeric check
	print("\nNumeric interp (range change, swapping mode and outvalues):")
	c = Quad_ease_in_out([-5,11], [[45,30,45], [255,128,0]])
	print(c)
	for i in [-5,-1,3,11]:
		print("at %2.2f: %s" %(i, c.interpolate(i)))
		c.mode = 'color'
		c.set_out([255,128,0],[45,30,45])
		print("at %2.2f: %s" %(i, c.interpolate(i)))
		c.mode = 'numeric'
		c.set_out([45,30,45], [255,128,0])
	
	d = Quad_ease_in_out([30,150], [[-50,50,0], [-1.0, 2.3, 4.8]])
	print(d, d.interpolate(0.5))
	d.set_out(10,20)
	print(d, d.interpolate(0.5))
	#
	# Generalised interpolator function
	print("Generalised Interpolator")
	e = Interpolator(Quad_ease_in_out, [30,150], [[-50,50,0], [-1.0, 2.3, 4.8]])
	print(e, e.interpolate(0.5))
	e.set_out(10,20)
	print(e, e.interpolate(0.5))
	