#!/usr/bin/python

# Aim:
# We want to be able to define animation (motion and other effects) in a simple way.
# So we:
# - have a number of interpolation curve methods (like slowinout or TCB splines), to control motion.
# - apply these to properties of objects that can be numerically manipulated.

# A Cue defines a time and a value at that time.
#  - Where value is one or more numeric values (e.g. list of three values for a colour or 3d rotation).
# A Sequence is an interpolation curve between one or more Cues.
#  - Either the same curve is used for all Cues. E.g. SlowIn followed by SlowIn, etc.
#    Or the curve is a TCB Spline and the Cues therefore represent control points on the curve.
# A Channel controls an object and applies one kind of motion to it.
#  - it is composed of Sequences.
#  - doing a single kind of animation (e.g. motion, or color change, or...).
# A Sequencer controls a number of channels.
#  - each Sequencer evaluates all channels at a given time,
#  - combines operations into the minimum number of operations (e.g. by composing a Tmat). 
#  - Then performs them.
# This allows us to simply animate the properties of one or more objects.

# Additionally:
# A Sequence can be set to be relative or absolute(default).
#  - Absolute sets the properties value, a Relative sequence instead increments/decrements the value.
#  - E.g. adding relative Y translation noise to a simple rocket liftoff (absolute) slow_in Y movement.
# More than one Sequencer can be defined.
#  - All motions defined in a Sequencer are evaluated and performed once.

from numeric_interpolation import *

class Cue(object):
	def __init__(self, time, value=0, sequence=None):
		" value can be a tuple or list "
		self.time = time
		self.seconds = None # converted when seq added to channel
		self.value = value
	def __repr__(self):
		return "<Cue: t=%s v=%s(%s)>" %(self.time, self.value, self.seconds)

def make_sequences(curve, cues):
	" build adjacent sequences from cues "
	sequences = []
	if curve != 'cubic':
		# all require pairs
		idx = 0
		while idx < len(cues)-1:
			sequences.append(Sequence(curve, [cues[idx], cues[idx+1]]))
			idx+=1
	return sequences
	

class Sequence(object):
	def __init__(self, curve=None, cues = [], channel=None,):
		self.cues = cues
		self.curve = curve
		self.order_cues()
		self.channel = channel
		self.interpolator = None
		self.build_interpolator(curve)
		self.active = True
	
	def __repr__(self):
		return "<Seq: %d cues %s>" %(len(self.cues), self.interpolator)
	
	def order_cues(self):
		sorted(self.cues, key=lambda cue: cue.time)

	def normalise_cues(self, ticks_per_sec):
		" determine seconds from times based on Sequencer timebase "
		for cue in self.cues:
			cue.seconds = cue.time /  float(ticks_per_sec)
	
	def build_interpolator(self, curv_func):
		times = [self.cues[0].seconds, self.cues[1].seconds]
		outrange = [self.cues[0].value, self.cues[1].value]
		print times
		if times[1]: # [0] might be 0 and so read as False
			self.interpolator = curv_func(times, outrange)
		
	def valid_time(self, time):
		# print "valid"
		# print "", self.cues[0].time, time, self.cues[-1].time
		if self.active and (self.cues[0].seconds <= time <= self.cues[-1].seconds):
			return True
		else:
			return False


class Channel(object):
	""" Animates a property of an object
		using a number of Sequences (holding curves)
		and sharing time Cues.
	"""
	def __init__(self, object, operation, sequences=None):
		self.object = object
		self.operation = operation
		self.sequences = sequences
		self.parent = None
		if self.parent:
			self.order_sequences()
		
	
	def __repr__(self):
		return "<Channel: %s %d seqs>" %(self.object, len(self.sequences))
	
	def set_parent(self, sequencer):
		self.parent = sequencer

	def order_sequences(self):
		" examine sequences placing them in time-order"
		if self.sequences:
			sorted(self.sequences, key=lambda seq: seq.cues[0].time)
			for seq in self.sequences:
				seq.normalise_cues(self.parent.ticks_per_second)
				seq.build_interpolator(seq.curve)
		
	def add_sequence(self, sequence):
		" must be added in order "
		self.sequences.append(sequence)
		self.order_sequences()
	
	def find_sequence(self, time):
		seq = False
		for s in self.sequences:
			if s.valid_time(time):
				seq = s
				break
		return seq	

	def perform(self, time):
		" time is in seconds from Sequencer start "
		seq = self.find_sequence(time)
		if seq:
			value = seq.interpolator.interpolate(time)
			self.operation(value)
			print "  Performed",self.object, value, seq


class Sequencer(object):
	""" Performs the animations described in Channels of
		Sequences separated by Cues.
	"""
	def __init__(self, ticks_sec, range = [0,100], channels=None, loop=False):
		self.start_time, self.end_time = range
		self.loop = loop # if true then loop time variable
		self.basetime = -1 # not started
		self.seq_time = 0 # tracks as we go
		self.ticks_per_second = ticks_sec
		self.channels = channels
		if channels:
			for c in channels:
				c.set_parent(self)
				c.order_sequences()

	def __repr__(self):
		return"<Sequencer: t=%s %d chan>"% (self.seq_time, len(self.channels))
	
	def add_channel(self, object=None):
		c = Channel(object)
		c.set_parent(self)
		self.channels.append(c)
		
	def start(self):
		" start the Sequence clock "
		self.basetime = 0
			
	def perform(self, delta_time):
		self.seq_time += delta_time
		print "Time=",self.seq_time#,self.end_time
		if self.loop and self.seq_time > self.end_time:
			self.seq_time = self.basetime
			print "Looping", self.basetime, self.seq_time
		# find time since Sequencer started -in seconds
		if self.basetime >=0:
			time = self.seq_time - self.basetime
			for c in self.channels:
				print " Peform: t=%s" %(time)
				c.perform(time)
		else:
			print "not started"


###
class OBJ(object):
	def __init__(self, name):
		self.pos = 0
		self.name = name
	def move(self, val):
		self.pos = val
	def __repr__(self):
		return "<OBJ %s pos=%s>" % (self.name, self.pos)


if __name__ == "__main__":
	A = OBJ("A")
	B = OBJ("B")
	Aop = A.move
	Bop = B.move
	#
	# csA = [[0, 2], [10, 4]] # [time,value]
	# csA2 = [[20, 4], [30, 2]] # [time,value]
	# csB = [[0, [2,10]], [10, [4,20]]] # [time,value]
	#
	csA = [[0, 2], [600, 4]]
	csA2 = [[1200, 4], [1800, 2]]
	csB = [[0, [2,10]], [600, [4,20]]]
	csA3 = [[0, 2], [600, 4], [1200, 4], [1800, 2]]
	#
	curv = QuadEaseInOut #Numeric_interpolator
	###
	cuesA = [Cue(t,v) for t,v in csA]
	cuesA2 = [Cue(t,v) for t,v in csA2]
	cuesA3 = [Cue(t,v) for t,v in csA3]
	print cuesA
	print cuesA2
	print cuesA3
	seqA1 = Sequence(curv, cuesA)
	seqA2 = Sequence(curv, cuesA2)
	print seqA1
	print seqA2
	cuesB = [Cue(t,v) for t,v in csB]
	seqB1 = Sequence(curv, cuesB)
	print cuesB
	print seqB1
	chanA = Channel(A, Aop, [seqA1, seqA2])
	chanB = Channel(B, Bop, [seqB1])
	chanC = Channel(A, Aop, make_sequences(curv, cuesA3))
	print chanA, chanB, chanC
	# S = Sequencer(60, [0,30], [chanA, chanB, chanC], False)
	S = Sequencer(60, [0,30], [chanC], False)
	for c in S.channels:
		for s in c.sequences:
			print s.cues
	S.start()
	print S
	print
	S.perform(0)
	S.perform(5) # now needs to be in delta time at 1fps or whetever
	S.perform(5)
	S.perform(5)
	S.perform(5)
	S.perform(5)
	S.perform(5)
	

## Easy input:
# want to be able to define:
# - [obj, op, curve, cues]
# where:
#   - cues have a tuple with same number of values as the op function consumes (test for this when construct)
#   - same curve is applied to all pairs of cues (or spline through knots)
# - [obj, op, [[curve, cues], ...]
# where its expected that the series of cues are in ascending order (not mandatory as will sort)

## Timebase:
# time defined in ms and s (see what pyglet uses now)
# defined per Sequencer
# call sequencer in window even tloop
# add S.xx to check game timer