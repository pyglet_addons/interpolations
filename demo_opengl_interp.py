#!/usr/bin/env python
# ----------------------------------------------------------------------------
# pyglet
# Copyright (c) 2006-2008 Alex Holkner
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions 
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright 
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of pyglet nor the names of its
#    contributors may be used to endorse or promote products
#    derived from this software without specific prior written
#    permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
# ----------------------------------------------------------------------------

"""Displays a rotating torus using the pyglet.graphics API.

This example uses the pyglet.graphics API to render an indexed vertex
list. The vertex list is added to a batch, allowing easy rendered
alongside other vertex lists with minimal overhead.

This example demonstrates:

 * Setting a 3D projection on a window by overriding the default
   on_resize handler
 * Enabling multisampling if available
 * Drawing simple 3D primitives using the pyglet.graphics API
 * Fixed-pipeline lighting
"""

from math import pi, sin, cos

import pyglet
from pyglet.gl import *
from numeric_interpolation import *

try:
    # Try and create a window with multisampling (antialiasing)
    config = Config(sample_buffers=1, samples=4, depth_size=16, double_buffer=True)
    window = pyglet.window.Window(resizable=True, config=config)
except pyglet.window.NoSuchConfigException:
    # Fall back to no multisampling for old hardware
    window = pyglet.window.Window(resizable=True)

# Change the window projection to 3D:
window.projection = pyglet.window.Projection3D()

### Position Interpolation
from pyglet.window import mouse, key
duration = 1 # seconds to move to clicked location
motion_curves = [ Numeric_interpolator, 
    Quad_ease_in_out, Quad_ease_in, Quad_ease_out,
    Cubic_ease_in, Cubic_ease_out, Cubic_ease_in_out,
    Quartic_ease_in, Quartic_ease_out, Quartic_ease_in_out,
    Quintic_ease_in, Quintic_ease_out, Quintic_ease_in_out,
    Sine_ease_in, Sine_ease_out, Sine_ease_in_out,
    Circular_ease_in, Circular_ease_out, Circular_ease_in_out,
    Exponential_ease_in, Exponential_ease_out, Exponential_ease_in_out,
    Elastic_ease_in, Elastic_ease_out, Elastic_ease_in_out,
    Back_ease_in, Back_ease_out, Back_ease_in_out,
    Bounce_ease_in, Bounce_ease_out, Bounce_ease_in_out]
motion_curve_id = 1
# start position
lastx = destx = window.width/2
lasty = desty = window.height/2
stepping = 1 # start stationary
# Each mover has inrange=[0,1]
x_screen_mover = Interpolator(motion_curves[motion_curve_id], [0,1], [lastx,lastx])
y_screen_mover = Interpolator(motion_curves[motion_curve_id], [0,1], [lasty,lasty])
stepping = 0
#
window.set_caption("interpolation=%s"%(motion_curves[motion_curve_id]))

@window.event
def on_resize(width, height):
    # reset the window width ranges
    # window.projection = pyglet.window.Projection3D()
    print("sizes", window.get_size())
    #return pyglet.event.EVENT_HANDLED

@window.event
def on_mouse_press(x, y, button, modifiers):
    " click Left to set new desired position "
    global x_screen_mover, stepping, destx, desty
    if button & mouse.LEFT:
        destx, desty = x, y
        x_screen_mover.set_out(lastx, destx)
        y_screen_mover.set_out(lasty, desty)
        stepping = 0 # restarts the movement
        
@window.event
def on_key_press(symbol, modifiers):
    " Use Left and Right arrow keys to step through curve choices "
    global motion_curve_id, x_screen_mover, y_screen_mover
    if symbol == key.RIGHT:
        motion_curve_id += 1
    if symbol == key.LEFT:
        motion_curve_id -= 1
    #
    if symbol == key.LEFT or symbol == key.RIGHT:
        motion_curve_id %= len(motion_curves)
        x_screen_mover = Interpolator(motion_curves[motion_curve_id], [0,1], [lastx,destx])
        y_screen_mover = Interpolator(motion_curves[motion_curve_id], [0,1], [lasty,desty])
        window.set_caption("interpolation=%s"%(motion_curves[motion_curve_id]))

        
@window.event
def on_draw():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()
    glTranslatef(tx, ty, -4)
    glRotatef(rz, 0, 0, 1)
    glRotatef(ry, 0, 1, 0)
    glRotatef(rx, 1, 0, 0)
    batch.draw()


def update(dt):
    global rx, ry, rz, tx, ty, stepping, lastx, lasty
    rx += dt * 1
    ry += dt * 80
    rz += dt * 30
    rx %= 360
    ry %= 360
    rz %= 360
    if stepping < 1:
        stepping += dt/duration
        # simple mapping of screen coords to [-4..4] range - for this object.
        tx = (x_screen_mover.interpolate(stepping) - window.width/2) /100
        ty = (y_screen_mover.interpolate(stepping) - window.height/2) /100
    else:
        lastx,lasty = destx,desty


def setup():
    # One-time GL setup
    glClearColor(1, 1, 1, 1)
    glColor3f(1, 0, 0)
    glEnable(GL_DEPTH_TEST)
    glEnable(GL_CULL_FACE)

    # Uncomment this line for a wireframe view
    #glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)

    # Simple light setup.  On Windows GL_LIGHT0 is enabled by default,
    # but this is not the case on Linux or Mac, so remember to always 
    # include it.
    glEnable(GL_LIGHTING)
    glEnable(GL_LIGHT0)
    glEnable(GL_LIGHT1)


def create_torus(radius, inner_radius, slices, inner_slices, batch):

    # Create the vertex and normal arrays.
    vertices = []
    normals = []

    u_step = 2 * pi / (slices - 1)
    v_step = 2 * pi / (inner_slices - 1)
    u = 0.
    for i in range(slices):
        cos_u = cos(u)
        sin_u = sin(u)
        v = 0.
        for j in range(inner_slices):
            cos_v = cos(v)
            sin_v = sin(v)

            d = (radius + inner_radius * cos_v)
            x = d * cos_u
            y = d * sin_u
            z = inner_radius * sin_v

            nx = cos_u * cos_v
            ny = sin_u * cos_v
            nz = sin_v

            vertices.extend([x, y, z])
            normals.extend([nx, ny, nz])
            v += v_step
        u += u_step

    # Create a list of triangle indices.
    indices = []
    for i in range(slices - 1):
        for j in range(inner_slices - 1):
            p = i * inner_slices + j
            indices.extend([p, p + inner_slices, p + inner_slices + 1])
            indices.extend([p, p + inner_slices + 1, p + 1])

    # Create a Material and Group for the Model
    diffuse = [0.5, 0.0, 0.3, 1.0]
    ambient = [0.5, 0.0, 0.3, 1.0]
    specular = [1.0, 1.0, 1.0, 1.0]
    emission = [0.0, 0.0, 0.0, 1.0]
    shininess = 50
    material = pyglet.model.Material("", diffuse, ambient, specular, emission, shininess)
    group = pyglet.model.MaterialGroup(material=material)

    vertex_list = batch.add_indexed(len(vertices)//3,
                                    GL_TRIANGLES,
                                    group,
                                    indices,
                                    ('v3f/static', vertices),
                                    ('n3f/static', normals))

    return pyglet.model.Model([vertex_list], [group], batch)


setup()
batch = pyglet.graphics.Batch()
torus_model = create_torus(1, 0.3, 50, 30, batch=batch)
rx = ry = rz = 0
tx = ty = 0

pyglet.clock.schedule(update)
print("sizes", window.get_size())
print(window.get_viewport_size())

pyglet.app.run()
