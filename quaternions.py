#!/usr/bin/python

# Quaternions
# Euler and axis-angle to Quaternion conversions
# Slerp provided

from __future__ import division
from __future__ import print_function

from math import sqrt,sin,cos,acos,asin,atan2,pi,radians,degrees

class Quat(object):
	def __init__(self, x=0,y=0,z=0,w=1):
		" first arg (x) can be a list of four. e;se 4 sep values"
		if type(x) == list:
			self.x = x[0]
			self.y = x[1]
			self.z = x[2]
			self.w = x[3]
		else:
			self.x = x
			self.y = y
			self.z = z
			self.w = w
			
	def __repr__(self):
		return ("<Quat: %s %s %s %s>"% (self.x, self.y, self.z, self.w))
	
	def length_sq(self):
		x = self.x
		y = self.y
		z = self.z
		w = self.w
		return x * x + y * y + z * z + w * w;
	
	def length(self):
		" Quat(3, 4, 0) output=5"
		return sqrt(self.length_sq());
	
	def __mul__(self, b):
		newq = Quat()
		q1x = self.x
		q1y = self.y
		q1z = self.z
		q1w = self.w
		q2x = b.x
		q2y = b.y
		q2z = b.z
		q2w = b.w
		#
		newq.x = q1w * q2x + q1x * q2w + q1y * q2z - q1z * q2y
		newq.y = q1w * q2y + q1y * q2w + q1z * q2x - q1x * q2z
		newq.z = q1w * q2z + q1z * q2w + q1x * q2y - q1y * q2x
		newq.w = q1w * q2w - q1x * q2x - q1y * q2y - q1z * q2z
		return newq
		
	def normalise(self):
		len = this.length()
		if (len == 0):
			this.x = this.y = this.z = 0
			this.w = 1
		else:
			len = 1 / len
			self.x *= len
			self.y *= len
			self.z *= len
			self.w *= len
		return this
	
	def set_from_axis_angle(self, axis, angle):
		a = 0.5 * radians(angle)
		sa = sin(a)
		self.x = sa * axis[0] # x
		self.y = sa * axis[1]
		self.z = sa * axis[2]
		self.w = cos(a)
		return self

	def set_from_euler_angles(self, ex, ey, ez):
		ex = 0.5 * radians(ex)
		ey = 0.5 * radians(ey)
		ez = 0.5 * radians(ez)

		sx = sin(ex)
		cx = cos(ex)
		sy = sin(ey)
		cy = cos(ey)
		sz = sin(ez)
		cz = cos(ez)

		self.x = sx * cy * cz - cx * sy * sz
		self.y = cx * sy * cz + sx * cy * sz
		self.z = cx * cy * sz - sx * sy * cz
		self.w = cx * cy * cz + sx * sy * sz

		return self
		
	def slerp(self, lhs, rhs, alpha):
		""" q1 = Quat(-0.11,-0.15,-0.46,0.87)
			q2 = Quat(-0.21,-0.21,-0.67,0.68)
			slerp(q1, q2, 0)	// Return q1
			slerp(q1, q2, 1)	// Return q2
		"""
		lx = lhs.x
		ly = lhs.y
		lz = lhs.z
		lw = lhs.w
		rx = rhs.x
		ry = rhs.y
		rz = rhs.z
		rw = rhs.w

		# Calculate angle between them.
		cosHalfTheta = lw * rw + lx * rx + ly * ry + lz * rz

		if (cosHalfTheta < 0):
			rw = -rw
			rx = -rx
			ry = -ry
			rz = -rz
			cosHalfTheta = -cosHalfTheta

		# If lhs == rhs or lhs == -rhs then theta == 0 and we can return lhs
		if (abs(cosHalfTheta) >= 1):
			self.w = lw
			self.x = lx
			self.y = ly
			self.z = lz
			return self

		# Calculate temporary values.
		halfTheta = acos(cosHalfTheta)
		sinHalfTheta = sqrt(1 - cosHalfTheta * cosHalfTheta)

		# If theta = 180 degrees then result is not fully defined
		# we could rotate around any axis normal to qa or qb
		if (abs(sinHalfTheta) < 0.001):
			self.w = (lw * 0.5 + rw * 0.5)
			self.x = (lx * 0.5 + rx * 0.5)
			self.y = (ly * 0.5 + ry * 0.5)
			self.z = (lz * 0.5 + rz * 0.5)
			return self

		ratioA = sin((1 - alpha) * halfTheta) / sinHalfTheta
		ratioB = sin(alpha * halfTheta) / sinHalfTheta

		# Calculate Quaternion.
		self.w = (lw * ratioA + rw * ratioB)
		self.x = (lx * ratioA + rx * ratioB)
		self.y = (ly * ratioA + ry * ratioB)
		self.z = (lz * ratioA + rz * ratioB)
		return self
	
	def get_euler_angles(self, eulers):
		if not eulers:
			eulers = [0,0,0]
		qx = self.x
		qy = self.y
		qz = self.z
		qw = self.w

		a2 = 2 * (qw * qy - qx * qz)
		if (a2 <= -0.99999):
			x = 2 * atan2(qx, qw)
			y = -pi / 2
			z = 0
		elif (a2 >= 0.99999):
			x = 2 * atan2(qx, qw)
			y = pi / 2
			z = 0
		else:
			x = atan2(2 * (qw * qx + qy * qz), 1 - 2 * (qx * qx +qy * qy))
			y = asin(a2)
			z = atan2(2 * (qw * qz + qx * qy), 1 - 2 * (qy * qy + qz * qz))

		return [degrees(x),degrees(y),degrees(z)]

	def get_axis_angle(self, axis=[1,0,0]):
		" set_from_axis angle([0, 1, 0], 90)"
		rad = acos(self.w) * 2
		s = sin(rad / 2)
		if (s != 0):
			axis[0] = self.x / s;
			axis[1] = self.y / s;
			axis[2] = self.z / s;
			if (axis[0] < 0 or axis[1] < 0 or axis[2] < 0):
				# Flip the sign
				axis[0] *= -1
				axis[1] *= -1
				axis[2] *= -1
				rad *= -1
		else:
			# If s is zero, return any axis (no rotation - axis does not matter)
			axis[0] = 1
			axis[1] = 0
			axis[2] = 0
		return degrees(rad)

### Testing
if __name__ == "__main__":
	a = Quat()
	print(a)
	b = Quat(-0.11,-0.15,-0.46,0.87)
	c = Quat(-0.21,-0.21,-0.67,0.68)
	d = Quat(0,0,0,5)
	print("length d = 5:", d.length())
	print("slerp 0	=",d.slerp(b,c,0))
	print("slerp 0.5=",d.slerp(b,c,0.5))
	print("slerp 1	=",d.slerp(b,c,1))
	d.set_from_axis_angle([0,1,0],90)
	print("axis angle = 90:",d.get_axis_angle([0,1,0]))
	print("euler angles = 90:",d.get_euler_angles(None))