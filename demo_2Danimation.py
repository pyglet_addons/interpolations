#!/usr/bin/env python
# ----------------------------------------------------------------------------
# pyglet
# Copyright (c) 2006-2008 Alex Holkner
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions 
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright 
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of pyglet nor the names of its
#    contributors may be used to endorse or promote products
#    derived from this software without specific prior written
#    permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
# ----------------------------------------------------------------------------

'''Load and display a GIF animation.

Usage::

    animation.py [<filename>]

If the filename is omitted, a sample animation is loaded
'''
from __future__ import print_function

__docformat__ = 'restructuredtext'
__version__ = '$Id$'

# The dinosaur.gif file packaged alongside this script is in the public
# domain, it was obtained from http://www.gifanimations.com/.


import sys
import pyglet
import numeric_interpolation as ni
from pyglet.window import mouse, key

duration = 1 # seconds to move to clicked location

if len(sys.argv) > 1:
    # Load the animation from file path.
    animation = pyglet.image.load_animation(sys.argv[1])
    bin = pyglet.image.atlas.TextureBin()
    animation.add_to_texture_bin(bin)
else:
    # Load animation from resource (this script's directory).
    animation = pyglet.resource.animation('resources/dinosaur.gif')
    # animation.anchor_x = animation.get_max_width()

	
### Position Interpolation
curves_ease_in_outs = [ ni.Numeric_interpolator, 
    ni.Quad_ease_out, ni.Cubic_ease_out, ni.Quartic_ease_out,
    ni.Quintic_ease_out, ni.Sine_ease_out, ni.Circular_ease_out,
    ni.Exponential_ease_out, ni.Elastic_ease_out, ni.Back_ease_out,
    ni.Bounce_ease_out
    ]
curves_ease_ins = [ ni.Numeric_interpolator, 
    ni.Quad_ease_in, ni.Cubic_ease_in, ni.Quartic_ease_in,
    ni.Quintic_ease_in, ni.Sine_ease_in, ni.Circular_ease_in,
    ni.Exponential_ease_in, ni.Elastic_ease_in, ni.Back_ease_in,
    ni.Bounce_ease_in
    ]
curves_ease_outs = [ ni.Numeric_interpolator, 
    ni.Quad_ease_in_out, ni.Cubic_ease_in_out, ni.Quartic_ease_in_out,
    ni.Quintic_ease_in_out, ni.Sine_ease_in_out, ni.Circular_ease_in_out,
    ni.Exponential_ease_in_out, ni.Elastic_ease_in_out, ni.Back_ease_in_out,
    ni.Bounce_ease_in_out
    ]
motion_sets = [curves_ease_ins, curves_ease_outs, curves_ease_in_outs]
# for labels
motion_names = ["Ease In Curves","Ease Out Curves","Ease In&Out Curves"]
motion_set = 0

# Labels


# Make sprites
sprite_height = animation.get_max_height()
sprite_width_2 = animation.get_max_width()/2
batch = pyglet.graphics.Batch()
sprites = []

# Window
window = pyglet.window.Window(height=len(curves_ease_ins)*sprite_height, resizable=True)
window.set_caption("interpolation=%s"%(motion_names[motion_set]))
# Set window background color to white.
pyglet.gl.glClearColor(1, 1, 1, 1)

# Build Sprites
for idx in range(len(motion_sets[motion_set])):
        sprites.append(pyglet.sprite.Sprite(animation, x=-sprite_width_2, y=sprite_height*idx, batch=batch))

# Build movers
def build_movers(start, end):
    movers = []
    for idx,curve in enumerate(motion_sets[motion_set]):
        movers.append(ni.Interpolator(curve, [0,1], [start, end]))
    return movers
movers = build_movers(0,window.width)

#
labels = []
for idx,curve in enumerate(motion_sets[motion_set]):
	name = curve.__name__
	labels.append(pyglet.text.Label(name[:name.find("_")],
					  font_name='Times New Roman',
					  font_size=20,
					  color = (0,0,0,255),
					  x=10, y=idx*sprite_height,
					  anchor_x='left', anchor_y='bottom'))

# start position
lastx = destx = 0
stepping = 1 # start stationary


@window.event
def on_mouse_press(x, y, button, modifiers):
    " click Left to set new desired position "
    global stepping, destx
    if button & mouse.LEFT:
        destx = x
        stepping = 0 # will change from 0..1
        for m in movers:
            m.set_out(lastx, destx)

@window.event           
def on_key_press(symbol, modifiers):
    " Use Left and Right arrow keys to step through curve sets "
    global motion_set, movers
    if symbol == key.RIGHT:
        motion_set += 1
    if symbol == key.LEFT:
        motion_set -= 1
    #
    if symbol == key.LEFT or symbol == key.RIGHT:
        motion_set %= len(motion_sets)
        movers = build_movers(lastx,destx)
        window.set_caption("interpolation=%s"%(motion_names[motion_set]))


def update(dt):
    global stepping, lastx
    if stepping < 1:
        stepping += dt/duration
        stepping = min(stepping, 1.0)
        # simple mapping of screen coords
        for s,m in zip(sprites, movers):
            s.x = m.interpolate(stepping) - sprite_width_2
    else:
        lastx = destx
    
pyglet.clock.schedule(update)


@window.event
def on_draw():
    window.clear()
    batch.draw()
    for ll in labels:
        ll.draw()

pyglet.app.run()
